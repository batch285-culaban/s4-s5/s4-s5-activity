import com.zuitt.example.Contact;
import com.zuitt.example.Phonebook;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args){

        Contact johnDoe = new Contact("John Doe",
                new ArrayList<>(Arrays.asList("+639152468596", "+639228547963")),
                new ArrayList<>(Arrays.asList("my home is Quezon City", "my office in Makati City")));
        Contact janeDoe = new Contact("Jane Doe",
                new ArrayList<>(Arrays.asList("+639162148573", "+639173698541")),
                new ArrayList<>(Arrays.asList("my home is Caloocan City", "my office in Pasay City")));


        Phonebook phonebook = new Phonebook();
        phonebook.addContact(johnDoe);
        phonebook.addContact(janeDoe);


        ArrayList<Contact> contacts = phonebook.getContacts();

        for (int i = 0; i < contacts.size(); i++) {
            Contact contact = contacts.get(i);
            System.out.println(contact.getName());
            System.out.println("-----------------");
            System.out.println(contact.getName() + " has the following registered numbers:");
            ArrayList<String> numbers = contact.getContactNumbers();
            for (int j = 0; j < numbers.size(); j++) {
                System.out.println(numbers.get(j));
            }
            System.out.println("-----------------------------------------------------");
            System.out.println(contact.getName() + " has the following registered addresses:");
            ArrayList<String> addresses = contact.getAddresses();
            for (int j = 0; j < addresses.size(); j++) {
                System.out.println(addresses.get(j));
            }
            System.out.println("=====================================================");
        }
    }
}
