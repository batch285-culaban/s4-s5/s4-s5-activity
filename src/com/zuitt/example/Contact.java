package com.zuitt.example;

import java.util.ArrayList;

public class Contact {

    private String name;
    private String contactNumber;
    private String address;
    private ArrayList<String> contactNumbers;
    private ArrayList<String> addresses;

    public Contact(){};

    public Contact(String name, ArrayList<String> contactNumbers, ArrayList<String> addresses){
        this.name = name;
        this.contactNumbers = contactNumbers;
        this.addresses = addresses;
    }

    public String getName(){
        return name;
    }

    // setters and getters:

    public void setName(String name){
        this.name = name;
    }

    public void setContactNumber(String contactNumber){
        this.contactNumber = contactNumber;
    }

    public void setAddress(String address){
        this.address = address;
    }

    public void setContactNumbers(ArrayList<String> contactNumbers){
        this.contactNumbers = contactNumbers;
    }

    public ArrayList<String> getContactNumbers() {
        return contactNumbers;
    }

    public void setAddresses(ArrayList<String> addresses){
        this.addresses = addresses;
    }

    public ArrayList<String> getAddresses() {
        return addresses;
    }
}
