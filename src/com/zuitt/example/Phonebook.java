package com.zuitt.example;

import java.util.ArrayList;

public class Phonebook {

    private ArrayList<Contact> contacts;

    public Phonebook(){
        this.contacts = new ArrayList<>();
    }

    public Phonebook(ArrayList<Contact> contacts){
        this.contacts = contacts;
    }

    // for adding new Contacts to our Array
    public void addContact(Contact contact){
        this.contacts.add(contact);
    }

    //setter and getter

    public void setContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public boolean isEmpty() {
        return contacts.isEmpty();
    }
}
